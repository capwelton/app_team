<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */


namespace Capwelton\App\Team\ComponentDefinition;

use Capwelton\App\Team\Ctrl\TeamController;
use Capwelton\App\Team\Ctrl\TeamTypeController;
use Capwelton\App\Team\Set\TeamMemberRoleSet;
use Capwelton\App\Team\Set\TeamSet;
use Capwelton\App\Team\Ui\TeamUi;
use Capwelton\App\Team\Set\TeamTypeSet;
use Capwelton\App\Team\Ui\TeamTypeUi;
use Capwelton\App\Team\Set\TeamRoleSet;
use Capwelton\App\Team\Ctrl\TeamRoleController;
use Capwelton\App\Team\Ui\TeamRoleUi;
use Capwelton\App\Team\Set\TeamMemberSet;
use Capwelton\App\Team\Set\TeamLinkSet;

class ComponentDefinition implements \app_ComponentDefinition
{   
    public function getDefinition()
    {
        return 'Manages teams';
    }
    
    public function getComponents(\Func_App $App)
    {
        $teamComponent = $App->createComponent(TeamSet::class, TeamController::class, TeamUi::class);
        $teamTypeComponent = $App->createComponent(TeamTypeSet::class, TeamTypeController::class, TeamTypeUi::class);
        $teamRoleComponent = $App->createComponent(TeamRoleSet::class, TeamRoleController::class, TeamRoleUi::class);
        $teamLinkCompoment = $App->createComponent(TeamLinkSet::class, null, null);
        $teamMemberComponent = $App->createComponent(TeamMemberSet::class, null, null);
        $teamMemberRoleComponent = $App->createComponent(TeamMemberRoleSet::class, null, null);
        
        return array(
            'TEAM' => $teamComponent,
            'TEAMTYPE' => $teamTypeComponent,
            'TEAMROLE' => $teamRoleComponent,
            'TEAMLINK' => $teamLinkCompoment,
            'TEAMMEMBER' => $teamMemberComponent,
            'TEAMMEMBERROLE' => $teamMemberRoleComponent
        );
    }

    public function getLangPath(\Func_App $App)
    {
        $addon = $App->getAddon();
        if(!$addon){
            return null;
        }
        return $addon->getPhpPath().'vendor/capwelton/appteam/src/langfiles/';
    }
    
    public function getStylePath(\Func_App $App)
    {
        $addon = $App->getAddon();
        if(!$addon){
            return null;
        }
        return $addon->getPhpPath().'vendor/capwelton/appteam/src/styles/';
    }
    
    public function getScriptPath(\Func_App $App)
    {
        return null;
    }
    public function getConfiguration(\Func_App $App)
    {
        $W = bab_Widgets();
        $component = $App->getComponentByName('Team');
        return array(
            array(
                'sectionName' => 'Main',
                'sectionContent' => array(
                    $W->Link(
                        $W->Icon($component->translate('Team types'), \Func_Icons::ACTIONS_VIEW_LIST_DETAILS),
                        $App->Controller()->TeamType()->displayList()
                    ),
                    $W->Link(
                        $W->Icon($component->translate('Team roles'), \Func_Icons::ACTIONS_VIEW_LIST_DETAILS),
                        $App->Controller()->TeamRole()->displayList()
                    ),
                    $W->Link(
                        $W->Icon($component->translate('Generic teams'), \Func_Icons::ACTIONS_VIEW_LIST_DETAILS),
                        $App->Controller()->Team()->displayList()
                    )
                )
            )
        );
    }
}