<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */


namespace Capwelton\App\Team\Set;

include_once 'base.php';


/**
 * @method TeamMember[] select()
 * @method TeamMember   get()
 * @method TeamMember   newRecord()
 * @method Func_App     App()
 * 
 * @property \ORM_StringField   $memberRef
 * @property TeamSet            $team
 * 
 * @method TeamSet      team()
 * @method TeamRoleSet  role()
 */
class TeamMemberSet extends \app_TraceableRecordSet
{
    public function __construct(\Func_App $App = null)
    {
        parent::__construct($App);
        
        $this->setTableName($App->classPrefix.'TeamMember');

        $this->setDescription('Team member');
        
        $this->setPrimaryKey('id');
        $appC = $App->getComponentByName('Team');

        $this->addFields(
            ORM_StringField('memberRef')->setDescription($appC->translate('Object reference of the member')),
            ORM_TextField('comment')
        );
        
        $this->hasOne('team', $App->TeamSetClassName());
    }
    
    /**
     *
     * {@inheritdoc}
     * @see \app_TraceableRecordSet::save()
     */
    public function save(\ORM_Record $record, $noTrace = false)
    {
        $event = new TeamMemberBeforeSaveEvent($record);
        bab_fireEvent($event);
        
        $result = parent::save($record);
        
        $event = new TeamMemberAfterSaveEvent($record);
        bab_fireEvent($event);
        
        return $result;
    }

    /**
     *
     * @return \ORM_Criteria
     */
    public function isReadable()
    {
        return $this->all();
    }

    /**
     *
     * @return \ORM_Criteria
     */
    public function isCreatable()
    {
        return $this->isUpdatable();
    }

    /**
     *
     * @return \ORM_Criteria
     */
    public function isUpdatable()
    {
        return $this->none();
    }

    /**
     *
     * @return \ORM_Criteria
     */
    public function isDeletable()
    {
        return $this->isUpdatable();
    }
}

class TeamMemberBeforeSaveEvent extends \RecordBeforeSaveEvent
{
    
}

class TeamMemberAfterSaveEvent extends \RecordAfterSaveEvent
{
    
}