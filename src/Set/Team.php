<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */


namespace Capwelton\App\Team\Set;

include_once 'base.php';

/**
 * @property string     $name   The name of the team
 * @property bool       $isGeneric Weither the team can be linked to multiple objects or not
 * @property TeamType   $type   The type of the team
 *
 * @method TeamSet      getParentSet()
 * @method TeamType     type()
 * @method \Func_App    App()
 */
class Team extends \app_TraceableRecord
{
    /**
     * @param boolean $withPast
     * @return TeamMember[]
     */
    public function getMembers($withPast = false)
    {
        $App = $this->App();
        /* @var $teamMemberSet TeamMemberSet */
        $teamMemberSet = $App->TeamMemberSet();
        /* @var $teamMemberRoleSet TeamMemberRoleSet */
        $teamMemberRoleSet = $App->TeamMemberRoleSet();
        
        $conditions = array($teamMemberSet->team->is($this->id));
        
        if(!$withPast){
            $now = date("Y-m-d");
            $conditions[] = $teamMemberSet->id->in(
                $teamMemberRoleSet->start->is('0000-00-00')->_OR_($teamMemberRoleSet->start->lessThanOrEqual($now)),
                'teamMember'
            );
            $conditions[] = $teamMemberSet->id->in(
                $teamMemberRoleSet->end->is('0000-00-00')->_OR_($teamMemberRoleSet->end->greaterThanOrEqual($now)),
                'teamMember'
            );
        }
        
        return $teamMemberSet->select($teamMemberSet->all($conditions));
    }
    
    /**
     * @param boolean $withPast
     * @return \app_Record[]
     */
    public function getMemberObjects($withPast = false)
    {
        $linkedMembers = array();
        
        $members = $this->getMembers($withPast);
        foreach ($members as $member){
            $linkedMembers[] = $member->member();
        }
        
        return $linkedMembers;
    }
    
    /**
     * @param boolean $withPast
     * @return TeamLink[]
     */
    public function getLinks($withPast = false)
    {
        $App = $this->App();
        /* @var $teamLinkSet TeamLinkSet */
        $teamLinkSet = $App->TeamLinkSet();
        
        $conditions = array($teamLinkSet->team->is($this->id));
        
        if(!$withPast){
            $now = date("Y-m-d");
            $conditions[] = $teamLinkSet->start->is('0000-00-00')->_OR_($teamLinkSet->start->lessThanOrEqual($now));
            $conditions[] = $teamLinkSet->end->is('0000-00-00')->_OR_($teamLinkSet->end->greaterThanOrEqual($now));
        }
        
        return $teamLinkSet->select($teamMemberSet->all($conditions));
    }
    
    /**
     * @param boolean $withPast
     * @return \app_Record[]
     */
    public function getLinkedObjects($withPast = false)
    {
        $linkedObjects = array();
        
        $links = $this->getLinks($withPast);
        foreach ($links as $link){
            $linkedObjects[] = $link->object();
        }
        
        return $linkedObjects;
    }
}
