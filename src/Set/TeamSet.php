<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */


namespace Capwelton\App\Team\Set;

include_once 'base.php';

/**
 * @property string         $name   The name of the team
 * @property bool           $isGeneric Wether the team is a generic one, and can be associated to multiple objects
 * @property TeamTypeSet    $type   The type of the team
 *
 * @method TeamSet      getParentSet()
 * @method TeamTypeSet  type()
 * @method Func_App     App()
 */
class TeamSet extends \app_TraceableRecordSet
{
    /**
     * @param \Func_App $App
     */
    public function __construct(\Func_App $App = null)
    {
        parent::__construct($App);
        
        $this->setTableName($App->classPrefix.'Team');
        
        $this->setDescription('Team');
        
        $this->setPrimaryKey('id');
        
        $appC = $App->getComponentByName('Team');
        
        $this->addFields(
            ORM_StringField('name')->setDescription($appC->translate('Name')),
            ORM_BoolField('isGeneric')->setOutputOptions($appC->translate('No'), $appC->translate('Yes'))
        );
        
        $this->hasOne('type', $App->TeamTypeSetClassName())->setDescription($App->translatable('Type'));
    }
    
    /**
     *
     * {@inheritdoc}
     * @see \app_TraceableRecordSet::save()
     */
    public function save(\ORM_Record $record, $noTrace = false)
    {
        $event = new TeamBeforeSaveEvent($record);
        bab_fireEvent($event);
        
        $result = parent::save($record);
        
        $event = new TeamAfterSaveEvent($record);
        bab_fireEvent($event);
        
        return $result;
    }
        
        
    
    public function getRequiredComponents()
    {
        return array(
            'CONTACT',
            'ORGANIZATION'
        );
    }
    
    /**
     *
     * @return \ORM_Criteria
     */
    public function isReadable()
    {
        return $this->deleted->is(\app_TraceableRecord::DELETED_STATUS_EXISTING);
    }
    
    /**
     *
     * @return \ORM_Criteria
     */
    public function isCreatable()
    {
        return $this->isUpdatable();
    }
    
    /**
     *
     * @return \ORM_Criteria
     */
    public function isUpdatable()
    {
        return $this->isReadable();
    }
    
    /**
     *
     * @return \ORM_Criteria
     */
    public function isDeletable()
    {
        return $this->isUpdatable();
    }
}

class TeamBeforeSaveEvent extends \RecordBeforeSaveEvent
{
    
}

class TeamAfterSaveEvent extends \RecordAfterSaveEvent
{
    
}