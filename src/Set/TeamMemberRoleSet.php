<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */


namespace Capwelton\App\Team\Set;

include_once 'base.php';


/**
 * @method TeamMemberRole[] select()
 * @method TeamMemberRole   get()
 * @method TeamMemberRole   newRecord()
 * @method Func_App         App()
 * 
 * @property \ORM_DateField     $start
 * @property \ORM_DateField     $end
 * @property \ORM_TextField     $comment
 * @property TeamMemberSet      $teamMember
 * @property TeamRoleSet        $teamRole
 * 
 * @method TeamMemberSet    teamMember()
 * @method TeamRoleSet      teamRole()
 */
class TeamMemberRoleSet extends \app_TraceableRecordSet
{
    public function __construct(\Func_App $App = null)
    {
        parent::__construct($App);
        
        $this->setTableName($App->classPrefix.'TeamMemberRole');

        $this->setDescription('Team member role');
        
        $this->setPrimaryKey('id');
        
        $appC = $App->getComponentByName('Team');

        $this->addFields(
            ORM_DateField('start')->setDescription($appC->translate('Start date')),
            ORM_DateField('end')->setDescription($appC->translate('End date')),
            ORM_TextField('comment')->setDescription($appC->translate('Comment'))
        );
        
        $this->hasOne('teamMember', $App->TeamMemberSetClassName());
        $this->hasOne('teamRole', $App->TeamRoleSetClassName());
    }
    
    /**
     *
     * {@inheritdoc}
     * @see \app_TraceableRecordSet::save()
     */
    public function save(\ORM_Record $record, $noTrace = false)
    {
        $event = new TeamMemberRoleBeforeSaveEvent($record);
        bab_fireEvent($event);
        
        $result = parent::save($record);
        
        $event = new TeamMemberRoleAfterSaveEvent($record);
        bab_fireEvent($event);
        
        return $result;
    }

    /**
     *
     * @return \ORM_Criteria
     */
    public function isReadable()
    {
        return $this->all();
    }

    /**
     *
     * @return \ORM_Criteria
     */
    public function isCreatable()
    {
        return $this->isUpdatable();
    }

    /**
     *
     * @return \ORM_Criteria
     */
    public function isUpdatable()
    {
        return $this->none();
    }

    /**
     *
     * @return \ORM_Criteria
     */
    public function isDeletable()
    {
        return $this->isUpdatable();
    }
}

class TeamMemberRoleBeforeSaveEvent extends \RecordBeforeSaveEvent
{
    
}

class TeamMemberRoleAfterSaveEvent extends \RecordAfterSaveEvent
{
    
}