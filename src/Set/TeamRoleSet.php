<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */


namespace Capwelton\App\Team\Set;

include_once 'base.php';


/**
 * @method TeamRole[]   select()
 * @method TeamRole     get()
 * @method TeamRole     newRecord()
 * @method Func_App     App()
 */
class TeamRoleSet extends \app_RecordSet
{
    public function __construct(\Func_App $App = null)
    {
        parent::__construct($App);
        
        $this->setTableName($App->classPrefix.'TeamRole');

        $this->setDescription('Team role');
        
        $this->setPrimaryKey('id');
        
        $appC = $App->getComponentByName('Team');

        $this->addFields(
            ORM_StringField('name')->setDescription($appC->translate('Name'))
        );
        
        $this->hasOne('parent', $App->TeamRoleSetClassName())->setDescription($App->translatable('Parent'));
    }
    
    /**
     *
     * {@inheritdoc}
     * @see \app_TraceableRecordSet::save()
     */
    public function save(\ORM_Record $record, $noTrace = false)
    {
        $event = new TeamRoleBeforeSaveEvent($record);
        bab_fireEvent($event);
        
        $result = parent::save($record);
        
        $event = new TeamRoleAfterSaveEvent($record);
        bab_fireEvent($event);
        
        return $result;
    }
    
    private function instanciateFirstRole()
    {
        $set = $this->App()->TeamRoleSet();
        if ($set->select()->count() == 0) {
            // We must have a root node with id = 1
            $status = $set->newRecord();
            $status->id = 1;
            $status->parent = 0;
            $status->save();
        }
    }
    
    public function onUpdate()
    {
        $this->instanciateFirstRole();
        
        $App = $this->app();
        $component = $App->getComponentByName('Team');
        $teamRoleSet = $App->TeamRoleSet();
        $created = 0;
        if($teamRoleSet->select($teamRoleSet->parent->is(1))->count() == 0){
            $defaultTeamRole = $teamRoleSet->newRecord();
            $defaultTeamRole->name = $component->translate('Member');
            $defaultTeamRole->parent = 1;
            $defaultTeamRole->save();
            $created++;
        }
        if($created > 0){
            $message = $component->translate('Default team roles initialized');
            $color = 'green';
            $message = "<span style='color:{$color};'>{$message}</span>";
            \bab_installWindow::message($message);
        }
    }

    /**
     *
     * @return \ORM_Criteria
     */
    public function isReadable()
    {
        return $this->all();
    }

    /**
     *
     * @return \ORM_Criteria
     */
    public function isCreatable()
    {
        return $this->isUpdatable();
    }

    /**
     *
     * @return \ORM_Criteria
     */
    public function isUpdatable()
    {
        return $this->none();
    }

    /**
     *
     * @return \ORM_Criteria
     */
    public function isDeletable()
    {
        return $this->isUpdatable();
    }
}

class TeamRoleBeforeSaveEvent extends \RecordBeforeSaveEvent
{
    
}

class TeamRoleAfterSaveEvent extends \RecordAfterSaveEvent
{
    
}