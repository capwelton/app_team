<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */


namespace Capwelton\App\Team\Set;

include_once 'base.php';

/**
 * @method \Func_App     App()
 * @method TeamMemberSet  getParentSet()
 * 
 * @property date           $start
 * @property date           $end
 * @property string         $comment
 * @property TeamMember     $teamMember
 * @property TeamRole       $teamRole
 * 
 * @method TeamMember   teamMember()
 * @method TeamRole     teamRole()
 */
class TeamMemberRole extends \app_TraceableRecord
{
    /**
     * @return bool
     */
    public function isReadable()
    {
        return true;
    }

    /**
     * @return bool
     */
    public function isUpdatable()
    {
        return true;
    }

    /**
     * @return bool
     */
    public function isDeletable()
    {
        return true;
    }
    
    public function getDatesString()
    {
        $App = $this->App();
        $appC = $App->getComponentByName('Team');
        
        $str = '';
        
        if($this->start == '0000-00-00' && $this->end == '0000-00-00'){
            //No dates set
            $str = $appC->translate('No dates defined');
        }
        elseif($this->start == '0000-00-00' && $this->end != '0000-00-00'){
            //Only end date set
            $str = sprintf($appC->translate('Until %s'), date('d/m/Y', strtotime($this->end)));
        }
        elseif($this->start != '0000-00-00' && $this->end == '0000-00-00'){
            //Only start date set
            $str = sprintf($appC->translate('Starting from %s'), date('d/m/Y', strtotime($this->start)));
        }
        else{
            //Both date set
            $str = sprintf($appC->translate('Starting from %s until %s'), date('d/m/Y', strtotime($this->start)), date('d/m/Y', strtotime($this->end)));
        }
        
        return $str;
    }
}
