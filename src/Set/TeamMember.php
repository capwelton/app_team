<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */


namespace Capwelton\App\Team\Set;

include_once 'base.php';

/**
 * @method \Func_App     App()
 * @method TeamMemberSet  getParentSet()
 * 
 * @property string     $memberRef
 * @property Team       $team
 * 
 * @method Team     team()
 * @method TeamRole role()
 */
class TeamMember extends \app_TraceableRecord
{
    /**
     * @return bool
     */
    public function isReadable()
    {
        return true;
    }

    /**
     * @return bool
     */
    public function isUpdatable()
    {
        return true;
    }

    /**
     * @return bool
     */
    public function isDeletable()
    {
        return true;
    }
   
    /**
     * @return \app_Record
     */
    public function member()
    {
        $App = $this->App();
        return $App->getRecordByRef($this->memberRef);
    }
    
    /**
     * 
     * @param boolean $withPast
     * @return TeamMemberRole[]
     */
    public function getRoles($withPast = false)
    {
        $App = $this->App();
        /* @var $teamMemberRoleSet TeamMemberRoleSet */
        $teamMemberRoleSet = $App->TeamMemberRoleSet();
        
        $conditions = array($teamMemberRoleSet->teamMember->is($this->id));
        if(!$withPast){
            $now = date('Y-m-d');
            $conditions[] = $teamMemberRoleSet->start->is('0000-00-00')->_OR_($teamMemberRoleSet->start->lessThanOrEqual($now)); 
            $conditions[] = $teamMemberRoleSet->end->is('0000-00-00')->_OR_($teamMemberRoleSet->end->greaterThanOrEqual($now)); 
        }
        
        return $teamMemberRoleSet->select($teamMemberRoleSet->all($conditions))->orderDesc($teamMemberRoleSet->start)->orderDesc($teamMemberRoleSet->end);
    }
}
