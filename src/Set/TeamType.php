<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */


namespace Capwelton\App\Team\Set;

use Capwelton\App\Team\Set\TeamTypeSet;

include_once 'base.php';

/**
 * @method Func_App     App()
 * @method TeamTypeSet  getParentSet()
 */
class TeamType extends \app_Record
{
    /**
     * @return bool
     */
    public function isReadable()
    {
        return true;
        $set = $this->getParentSet();
        return $set->select($set->isReadable()->is(true)->_AND_($set->id->is($this->id)))->count() == 1;
    }

    /**
     * @return bool
     */
    public function isUpdatable()
    {
        return true;
        $set = $this->getParentSet();
        return $set->select($set->isReadable()->is(true)->_AND_($set->id->is($this->id)))->count() == 1;
    }

    /**
     * @return bool
     */
    public function isDeletable()
    {
        return true;
        $set = $this->getParentSet();
        return $set->select($set->isReadable()->is(true)->_AND_($set->id->is($this->id)))->count() == 1;
    }
   
}
