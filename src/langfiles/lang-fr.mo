��    F      L  a   |              
             2     ;     G     W     ^     f     v     �     �     �     �     �     �          "     5     F     O     T  	   f  	   p     z     �     �     �     �     �     �  @   �  -        9     @     H     M     P     a  5   �     �     �     �     �     �  4   �      	  
   &	     1	     B	     \	  	   a	  
   k	  	   v	  
   �	     �	     �	     �	     �	     �	     
     +
  "   H
  '   k
     �
     �
     �
     �
     �
    �
     �     �  &   �               )     G     O     [     v  '   �     �      �     �  (     '   0  	   X     b     t     �     �     �     �     �     �                   	   ,     6     J  ?   `  3   �     �     �     �     �     �       5        T     c     i     p     |  9   �     �     �     �     �               &     7     I     Y     s  )   �     �  &   �     �          )     I     d     i     u     y          &          B                $      *   1                2          <   A           7   (              ?   6   :   -   	      "      )   /   +                      >   #       4   0          ;   %             E   8   
   ,                      C                 3      9                 5      !                       F   '   D   .   @      =                Add a new team Add a role Add new member to team Add role Add subrole Additional data Cancel Comment Confirm delete? Contact : %s Create a new generic team Create a new team Create a new team type Create a specific team Default team roles initialized Default team types initialized Delete Delete this member Delete this role Deletion Edit Edit generic team Edit role Edit team Edit team member Edit team type End End date General Generic team Generic teams If checked, this team will be able to be linked to other objects It will not be possible to undo this deletion Member Members Name No No dates defined Object reference of the member Object reference of the record this team is linked to Organization : %s Role Roles Save Show all (%d more) Sorry, You are not allowed to perform this operation Start Start date Starting from %s Starting from %s until %s Team Team name Team roles Team type Team types The member is mandatory The name is mandatory The team name must be specified The team role has been deleted The team role has been saved The type is mandatory This element does not exists This team does not have any member This team member does not have any role Type Until %s Yes from %s to %s Project-Id-Version: appteam
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-07-27 15:34+0200
Last-Translator: SI4YOU <contact@si-4you.com>
Language-Team: SI4YOU<contact@si-4you.com>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-Basepath: ..
X-Poedit-KeywordsList: libapp_translate;libapp_translate:1,2;translate:1,2;translate
X-Generator: Poedit 3.1
X-Poedit-SearchPath-0: .
 Ajouter une équipe Ajouter un rôle Ajouter un nouveau membre à l'équipe Ajouter un rôle Ajouter un sous-rôle Informations supplémentaires Annuler Commentaire Confirmer la suppression ? Contact : %s Créer une nouvelle équipe générique Créer une nouvelle équipe Créer un nouveau type d'équipe Créer une équipe spécifique Rôles d'équipe par défaut instanciés Types d'équipe par défaut instanciés Supprimer Retirer ce membre Supprimer ce rôle Suppression Modifier Modifier l'équipe générique Modifier le rôle Modifier l'équipe Modifier le membre de l'équipe Modifier  un type d'équipe Fin Date de fin Général Équipe générique Équipes génériques Si cochée, cette équipe pourra être liée à d'autres objets Il ne sera pas possible d'annuler cette suppression Membre Membres Nom Non Aucune dates définies Référence objet du membre Référence objet du record associé à cette équipe Organisme : %s Rôle Rôles Enregistrer Tout afficher (%d de plus) Vous n'êtes pas autorisés à réaliser cette opération Début Date de début Depuis le %s Depuis le %s jusqu'au %s Équipe Nom de l'équipe Rôles d'équipe Type de l'équipe Types d'équipe Le membre est obligatoire Le nom est obligatoire Le nom de l'équipe doit être spécifié Le rôle a été supprimé Le rôle d'équipe a été enregistré Le type est obligatoire Cet élément n'existe pas Cette équipe n'a aucun membres Ce membre n'a aucun rôles Type Jusqu'au %s Oui du %s au %s 