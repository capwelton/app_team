<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\Team\Ctrl;

use Capwelton\App\Team\Set\Team;
use Capwelton\App\Team\Set\TeamSet;
use Capwelton\App\Team\Set\TeamTypeSet;

$App = app_App();
$App->includeRecordController();
$App->setCurrentComponentByName('Team');

/**
 * This controller manages actions that can be performed on team types.
 *
 * @method \Func_App    App()
 */
class TeamTypeController extends \app_ComponentCtrlRecord
{
    /**
     * @isComponentController
     */
    public function __construct(\Func_App $app)
    {
        parent::__construct($app, $app->getComponentByName('TeamType'));
    }
    
    /**
     * Returns the criteria on the specified Set corresponding
     * to the filter array.
     *
     * @param TeamTypeSet   $teamTypeSet
     * @param array         $filter
     * @return \ORM_Criteria
     */
    protected function getFilterCriteria(TeamTypeSet $teamTypeSet, $filter)
    {
        // Initial conditions are base on read access rights.
        $conditions = new \ORM_TrueCriterion();
        
        if (isset($filter['name']) && !empty($filter['name'])) {
            $conditions = $conditions->_AND_(
                $teamTypeSet->name->contains($filter['name'])
            );
        }
        
        return $conditions;
    }
    
    protected function toolbar($tableView)
    {
        $toolbar = parent::toolbar($tableView);
        $W = bab_Widgets();
        $toolbar->addItem(
            $W->Link(
                $this->App()->getComponentByName('Team')->translate('Create a new team type'),
                $this->proxy()->edit()
            )->addClass('icon', \Func_Icons::ACTIONS_LIST_ADD)
            ->setOpenMode(\Widget_Link::OPEN_DIALOG_AND_RELOAD)
        );
        return $toolbar;
    }
    
    /**
     * Editor team type form
     *
     * @param int | null $teamType
     * @return \app_Page
     */
    public function edit($teamType = null, $errors = null)
    {
        $App = $this->App();
        $appC = $App->getComponentByName('Team');
        $Ui = $App->Ui();
        
        $page = $Ui->Page();
        
        $teamTypeSet = $App->TeamTypeSet();
        
        $page->addClass('app-page-editor');
        
        if (isset($teamType)) {
            $page->setTitle($appC->translate('Edit team type'));
            if (is_array($teamType)) {
                
            } else if (isset($teamType)) {
                $teamType = $teamTypeSet->request($teamType);
            }
        } else {
            $page->setTitle($appC->translate('Create a new team type'));
        }
        
        $editor = $Ui->TeamTypeEditor($teamType);
        $editor->isAjax = bab_isAjaxRequest();
        $editor->setName('teamType');
        
        $page->addItem($editor);
        
        return $page;
    }
    
    /**
     * @param array $teamType
     * @return boolean
     */
    public function save($teamType = null)
    {
        $App = $this->App();
        
        $data = $teamType;
        $teamTypeSet = $App->TeamTypeSet();
        
        if (isset($data['id'])) {
            $teamType = $teamTypeSet->request($data['id']);
        } else {
            $teamType = $teamTypeSet->newRecord();
        }
        
        $teamType->setFormInputValues($data);
        
        $teamType->save();
        
        $this->addReloadSelector('.depends-' . $this->getRecordClassName());
        $this->addReloadSelector('.depends-TeamTypeController_modelView');
        
        return true;
    }
    
    
    
    public function delete($teamType)
    {
        $this->requireDeleteMethod();
        $App = $this->App();
        
        $set = $App->TeamTypeSet();
        
        $teamType = $set->request($teamType);
        
        $set->delete($set->id->is($teamType->id));
        
        $this->addReloadSelector('.depends-' . $this->getRecordClassName());
        $this->addReloadSelector('.depends-TeamTypeController_modelView');
        
        return true;
    }
    
    /**
     * Does nothing and returns to the previous page.
     * @return bool
     */
    public function cancel()
    {
        return true;
    }
}