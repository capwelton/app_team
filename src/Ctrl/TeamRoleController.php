<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\Team\Ctrl;

$App = app_App();
$App->includeRecordController();
$App->setCurrentComponentByName('Team');

/**
 * This controller manages actions that can be performed on team roles.
 *
 * @method \Func_App    App()
 */
class TeamRoleController extends \app_ComponentCtrlRecord
{
    /**
     * @isComponentController
     */
    public function __construct(\Func_App $app)
    {
        parent::__construct($app, $app->getComponentByName('TeamRole'));
    }
    /**
     * @return \app_Page
     */
    public function displayList()
    {
        $App = $this->App();
        $appC = $App->getComponentByName('Team');
        $W = bab_Widgets();
        
        $page = $App->Ui()->Page();
        $page->addClass('app-page-editor');
        
        $page->addItem($W->Title($appC->translate('Team roles'), 1));
        $Ui = $App->Ui();
        
        $page->addItem($Ui->TeamRoleList());
        $page->setReloadAction($this->proxy()->displayList());
        $page->addClass('depends-teamRoleList');
        
        return $page;
    }
    
    /**
     * @return \app_Page
     */
    public function add($parentRole)
    {
        $App = $this->App();
        $appC = $App->getComponentByName('Team');
        $W = bab_Widgets();
        
        $roleSet = $App->TeamRoleSet();
        
        $page = $App->Ui()->Page();
        $page->addClass('app-page-editor');
        
        $page->addItem($W->Title($appC->translate('Add role'), 1));
        $Ui = $App->Ui();
        
        $roleEditor = $Ui->TeamRoleEditor();
        $roleEditor->setName('teamRole');
        
        $parentRole = $roleSet->get($parentRole);
        if (isset($parentRole->id)) {
            $roleEditor->addItem(
                $W->Hidden(null, 'parent', $parentRole->id)
            );
        }
        
        $roleEditor->setSaveAction($this->proxy()->save());
        $roleEditor->setCancelAction($this->proxy()->cancel());
        
        $page->addItem($roleEditor);
        
        return $page;
    }
    
    /**
     * @return \app_Page
     */
    public function edit($role)
    {
        $App = $this->App();
        $appC = $App->getComponentByName('Team');
        $W = bab_Widgets();
        
        $roleSet = $App->TeamRoleSet();
        $role = $roleSet->get($role);
        
        $page = $App->Ui()->Page();
        $page->addClass('app-page-editor');
        
        $page->addItem($W->Title($appC->translate('Edit role'), 1));
        $Ui = $App->Ui();
        
        $roleEditor = $Ui->TeamRoleEditor();
        $roleEditor->setName('teamRole');
        if (isset($role)) {
            $roleEditor->addItem(
                $W->Hidden(null, 'id', $role->id)
            );
            $roleEditor->setRecord($role);
        }
        
        $roleEditor->setSaveAction($this->proxy()->save());
        $roleEditor->setCancelAction($this->proxy()->cancel());
        
        $page->addItem($roleEditor);
        
        return $page;
    }
    
    /**
     * @param array $teamRole
     * @return boolean
     */
    public function save($teamRole = null)
    {
        $App = $this->App();
        $appC = $App->getComponentByName('Team');
        
        $data = $teamRole;
        $teamRoleSet = $App->TeamRoleSet();
        
        if (isset($data['id'])) {
            $teamRole = $teamRoleSet->request($data['id']);
            $teamRole->setFormInputValues($data);
            $teamRole->save();
        } else {
            $teamRole = $teamRoleSet->newRecord();
            $teamRole->setFormInputValues($data);
            $teamRole->save();
        }
        
        $this->addMessage($appC->translate('The team role has been saved'));
        app_redirect($this->proxy()->displayList());
        
        return true;
    }
    
    /**
     * Displays a page asking to confirm the deletion of a record.
     *
     * @param int $id
     */
    public function confirmDelete($id)
    {
        $W = bab_Widgets();
        $App = $this->App();
        $appC = $App->getComponentByName('Team');
        $Ui = $App->Ui();
        
        $recordSet = $this->getRecordSet();
        $record = $recordSet->get($id);
        
        $page = $Ui->Page();
        
        $page->addClass('app-page-editor');
        $page->setTitle($appC->translate('Deletion'));
        
        if (!isset($record)) {
            $page->addItem(
                $W->Label($appC->translate('This element does not exists'))
            );
            $page->addClass('alert', 'alert-warning');
            return $page;
        }
        
        $form = new \app_Editor($App);
        
        $form->addItem($W->Hidden()->setName('id'));
        
        $recordTitle = $record->getRecordTitle();
        
        $subTitle = $appC->translate($recordSet->getDescription()) . ' "' . $recordTitle . '"';
        $form->addItem($W->Title($subTitle, 5));
        
        $form->addItem($W->Title($appC->translate('Confirm delete?'), 6));
        
        $form->addItem($W->Html(bab_toHtml($appC->translate('It will not be possible to undo this deletion'))));
        
        $confirmedAction = $this->proxy()->delete($id);
        $parameters = $confirmedAction->getParameters();
        foreach ($parameters as $key => $value) {
            $form->setHiddenValue($key, $value);
        }
        $form->addButton(
            $W->SubmitButton()
            ->setAction($confirmedAction)
            ->setLabel($appC->translate('Delete'))
            );
        $page->addItem($form);
        
        return $page;
    }
    
    public function delete($role)
    {
        $this->requireDeleteMethod();
        $App = $this->App();
        $appC = $App->getComponentByName('Team');
        
        $set = $App->TeamRoleSet();
        $role = $set->request($set->id->is($role));
        $set->delete($set->id->is($role->id));
        
        $this->addMessage($appC->translate('The team role has been deleted'));
        app_redirect($this->proxy()->displayList());
        
        return true;
    }
    
    /**
     * Does nothing and returns to the previous page.
     * @return bool
     */
    public function cancel()
    {
        return true;
    }
}