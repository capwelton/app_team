<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\Team\Ctrl;

use Capwelton\App\Team\Set\Team;
use Capwelton\App\Team\Set\TeamLinkSet;
use Capwelton\App\Team\Set\TeamSet;
use Capwelton\App\Team\Ui\TeamEditor;
use Capwelton\App\Team\Ui\TeamLinkItem;
use Capwelton\App\Team\Set\TeamMemberSet;
use Capwelton\App\Team\Set\TeamMember;
use Capwelton\App\Team\Ui\TeamMemberEditor;
use Capwelton\App\Team\Set\TeamMemberRoleSet;
use Capwelton\App\Team\Set\TeamLink;
use Capwelton\App\Team\Set\TeamTypeSet;

$App = app_App();
$App->includeRecordController();
$App->setCurrentComponentByName('Team');

/**
 * This controller manages actions that can be performed on teams.
 */
class TeamController extends \app_ComponentCtrlRecord
{
    /**
     * @isComponentController
     */
    public function __construct(\Func_App $app)
    {
        parent::__construct($app, $app->getComponentByName('Team'));
    }
    
    protected function toolbar($tableView)
    {
        $toolbar = parent::toolbar($tableView);
        $appC = $this->App()->getComponentByName('Team');
        
        $W = bab_Widgets();
        $toolbar->addItem(
            $W->Link(
                $appC->translate('Create a new generic team'),
                $this->proxy()->editGenericTeam()
            )->addClass('icon', \Func_Icons::ACTIONS_LIST_ADD)
            ->setOpenMode(\Widget_Link::OPEN_DIALOG_AND_RELOAD)
        );
        return $toolbar;
    }
    
    /**
     * Editor generic team form
     *
     * @param int | null $team
     * @return \app_Page
     */
    public function editGenericTeam($team = null)
    {
        $App = $this->App();
        $appC = $App->getComponentByName('Team');
        $Ui = $App->Ui();
        
        $page = $Ui->Page();
        
        $teamSet = $App->TeamSet();
        
        $page->addClass('app-page-editor');
        
        if (isset($team)) {
            $page->setTitle($appC->translate('Edit generic team'));
            if (is_array($team)) {
                
            } else if (isset($team)) {
                $team = $teamSet->request($team);
            }
        } else {
            $page->setTitle($appC->translate('Create a new generic team'));
            $team = $teamSet->newRecord();
            $team->deleted = \app_TraceableRecord::DELETED_STATUS_DRAFT;
            $team->save();
        }
        
        $page->addItem($this->recordEditor($team, true));
        
        return $page;
    }
    
    /**
     * Editor team form
     *
     * @param int | null $team
     * @return \app_Page
     */
    public function edit($team = null)
    {
        $App = $this->App();
        $appC = $App->getComponentByName('Team');
        $Ui = $App->Ui();
        
        $page = $Ui->Page();
        
        $teamSet = $App->TeamSet();
        
        $page->addClass('app-page-editor');
        
        if (isset($team)) {
            $page->setTitle($appC->translate('Edit team'));
            if (is_array($team)) {
                
            } else if (isset($team)) {
                $team = $teamSet->request($team);
            }
        } else {
            $page->setTitle($appC->translate('Create a new team'));
            $team = $teamSet->newRecord();
            $team->deleted = \app_TraceableRecord::DELETED_STATUS_DRAFT;
            $team->save();
        }
        
        $page->addItem($this->recordEditor($team));
        
        return $page;
    }
    
    /**
     * {@inheritDoc}
     * @see \app_CtrlRecord::recordEditor()
     * @return TeamEditor
     */
    protected function recordEditor($team = null, $forGenericTeam = false)
    {
        $editor = $this->App()->Ui()->TeamEditor($team, $forGenericTeam);
        $editor->isAjax = bab_isAjaxRequest();
        $editor->setName('team');
        return $editor;
    }
    
    public function membersBox($teamId, $displayOnly = false, $itemId = null)
    {
        $App = $this->App();
        $W = bab_Widgets();
        
        $box = $W->FlowLayout($itemId);
        $box->addClass('depends-teamMembers');
        $box->setIconFormat(16, 'left');
        $box->setReloadAction($this->proxy()->membersBox($teamId, $displayOnly, $box->getId()));
        
        $teamSet = $App->TeamSet();
        $teamSet->setDefaultCriteria($teamSet->deleted->in(array(\app_TraceableRecord::DELETED_STATUS_DRAFT, \app_TraceableRecord::DELETED_STATUS_EXISTING)));
        
        /* @var $team Team */
        $team = $teamSet->get($teamSet->id->is($teamId));
        
        if(!isset($team)){
            return $box;
        }
        
        $table = $App->Ui()->TeamMembersTable($team, $displayOnly);
        
        $box->addItem($table);
        return $box;
    }
    
    /**
     * @param array $teamType
     * @return boolean
     */
    public function save($team = null)
    {
        $App = $this->App();
        
        $data = $team;
        $teamSet = $App->TeamSet();
        $teamSet->setDefaultCriteria($teamSet->deleted->in(array(\app_TraceableRecord::DELETED_STATUS_DRAFT, \app_TraceableRecord::DELETED_STATUS_EXISTING)));
        
        /* @var $team Team */
        
        if (isset($data['id'])) {
            $team = $teamSet->request($data['id']);
        } else {
            $team = $teamSet->newRecord();
        }
        
        $team->setFormInputValues($data);
        
        if(isset($data['forGenericTeam']) && $data['forGenericTeam']){
            $team->isGeneric = true;
        }
        
        $team->deleted = \app_TraceableRecord::DELETED_STATUS_EXISTING;
        $team->save();
        
        $this->addReloadSelector('.depends-' . $this->getRecordClassName());
        $this->addReloadSelector('.depends-TeamController_modelView');
        
        return true;
    }
    
    /**
     * Does nothing and returns to the previous page.
     * @return bool
     */
    public function cancel()
    {
        return true;
    }
    
    public function addNewMember($team)
    {
        $App = $this->App();
        $appC = $App->getComponentByName('Team');
        $Ui = $App->Ui();
        
        $page = $Ui->Page();
        
        $teamSet = $App->TeamSet();
        $teamSet->setDefaultCriteria($teamSet->deleted->in(array(\app_TraceableRecord::DELETED_STATUS_DRAFT, \app_TraceableRecord::DELETED_STATUS_EXISTING)));
        
        $page->addClass('app-page-editor');
        $page->setTitle($appC->translate('Add new member to team'));
        
        $team = $teamSet->request($team);
        
        /* @var $teamMemberSet TeamMemberSet */
        $teamMemberSet = $App->TeamMemberSet();
        $teamMember = $teamMemberSet->newRecord();
        $teamMember->team = $team->id;
        $teamMember->deleted = \app_TraceableRecord::DELETED_STATUS_DRAFT;
        $teamMember->save();
        
        /* @var $editor TeamMemberEditor */
        $editor = $Ui->TeamMemberEditor($teamMember);
        $editor->isAjax = bab_isAjaxRequest();
        $editor->setName('teamMember');
        $editor->setAjaxAction($this->proxy()->saveTeamMemberDraft(), '', 'change');
        $editor->addClass('widget-no-close');
        
        $page->addItem($editor);
        
        return $page;
    }
    
    public function editMember($teamMember)
    {
        $App = $this->App();
        $appC = $App->getComponentByName('Team');
        $Ui = $App->Ui();
        
        $page = $Ui->Page();
        
        $teamMemberSet = $App->TeamMemberSet();
        $teamMemberSet->setDefaultCriteria($teamMemberSet->deleted->in(array(\app_TraceableRecord::DELETED_STATUS_DRAFT, \app_TraceableRecord::DELETED_STATUS_EXISTING)));
        
        $page->addClass('app-page-editor');
        $page->setTitle($appC->translate('Edit team member'));
        
        $teamMember = $teamMemberSet->request($teamMember);
        
        /* @var $editor TeamMemberEditor */
        $editor = $Ui->TeamMemberEditor($teamMember);
        $editor->isAjax = bab_isAjaxRequest();
        $editor->setName('teamMember');
        $editor->setAjaxAction($this->proxy()->saveTeamMemberDraft(), '', 'change');
        $editor->addClass('widget-no-close');
        
        $page->addItem($editor);
        
        return $page;
    }
    
    public function saveTeamMember($teamMember = null)
    {
        $App = $this->App();
        
        $data = $teamMember;
        $teamMemberSet = $App->TeamMemberSet();
        $teamMemberSet->setDefaultCriteria($teamMemberSet->deleted->in(array(\app_TraceableRecord::DELETED_STATUS_DRAFT, \app_TraceableRecord::DELETED_STATUS_EXISTING)));
        
        /* @var $teamMember TeamMember */
        $teamMember = $teamMemberSet->request($data['id']);
        
        $teamMember->setFormInputValues($data);
        $teamMember->deleted = \app_TraceableRecord::DELETED_STATUS_EXISTING;
        $teamMember->save();
        
        $this->addReloadSelector('.depends-' . $this->getRecordClassName());
        $this->addReloadSelector('.depends-teamMembers');
        $this->addReloadSelector('.depends-TeamController_modelView');
        
        return true;
    }
    
    public function saveTeamMemberDraft($teamMember = null)
    {
        $App = $this->App();
        
        $data = $teamMember;
        $teamMemberSet = $App->TeamMemberSet();
        $teamMemberSet->setDefaultCriteria($teamMemberSet->deleted->in(array(\app_TraceableRecord::DELETED_STATUS_DRAFT, \app_TraceableRecord::DELETED_STATUS_EXISTING)));
        
        /* @var $teamMember TeamMember */
        $teamMember = $teamMemberSet->request($data['id']);
        
        $teamMember->setFormInputValues($data);
        $teamMember->save();
        
        $needsReload = false;
        
        /* @var $teamMemberRoleSet TeamMemberRoleSet */
        $teamMemberRoleSet = $App->TeamMemberRoleSet();
        
        foreach ($data['memberRoles'] as $teamMemberRoleId => $teamMemberRoleData){
            switch ($teamMemberRoleId){
                case 'new':
                    if(!isset($teamMemberRoleData['teamRole']) || $teamMemberRoleData['teamRole'] == 0){
                        continue;
                    }
                    $teamMemberRole = $teamMemberRoleSet->newRecord();
                    $teamMemberRole->teamMember = $teamMember->id;
                    $teamMemberRole->teamRole = $teamMemberRoleData['teamRole'];
                    $teamMemberRole->start = date('Y-m-d');
                    $teamMemberRole->save();
                    $needsReload = true;
                    break;
                default:
                    $teamMemberRole = $teamMemberRoleSet->get($teamMemberRoleSet->id->is($teamMemberRoleId));
                    if(!isset($teamMemberRole)){
                        continue;
                    }
                    $previousValues = $teamMemberRole->getValues();
                    $teamMemberRole->setFormInputValues($teamMemberRoleData);
                    if($previousValues['teamRole'] != $teamMemberRole->teamRole || $previousValues['start'] != $teamMemberRole->start || $previousValues['end'] != $teamMemberRole->end){
                        $needsReload = true;
                    }
                    $teamMemberRole->save();
                    break;
            }
        }
        
        if($needsReload){
            $this->addReloadSelector('.depends-' . $this->getRecordClassName());
            $this->addReloadSelector('.depends-teamMembers');
            $this->addReloadSelector('.depends-teamMemberRoles');
            $this->addReloadSelector('.depends-TeamController_modelView');
        }
        
        return true;
    }
    
    public function confirmDeleteTeamMember($teamMember)
    {
        $W = bab_Widgets();
        $App = $this->App();
        $appC = $App->getComponentByName('Team');
        $Ui = $App->Ui();
        
        $recordSet = $App->TeamMemberSet();
        $record = $recordSet->get($recordSet->id->is($teamMember));
        
        $page = $Ui->Page();
        
        $page->addClass('app-page-editor');
        $page->setTitle($appC->translate('Deletion'));
        
        if (!isset($record)) {
            $page->addItem(
                $W->Label($appC->translate('This element does not exists'))
            );
            $page->addClass('alert', 'alert-warning');
            return $page;
        }
        
        
        $form = new \app_Editor($App);
        
        $form->addItem($W->Hidden()->setName('id'));
        
        $recordTitle = $record->getRecordTitle();
        
        $subTitle = $appC->translate($recordSet->getDescription()) . ' "' . $recordTitle . '"';
        $form->addItem($W->Title($subTitle, 5));
        
        $form->addItem($W->Title($appC->translate('Confirm delete?'), 6));
        
        $form->addItem($W->Html(bab_toHtml($appC->translate('It will not be possible to undo this deletion'))));
        
        $confirmedAction = $this->proxy()->deleteTeamMember($teamMember);
        $parameters = $confirmedAction->getParameters();
        foreach ($parameters as $key => $value) {
            $form->setHiddenValue($key, $value);
        }
        $form->addButton(
            $W->SubmitButton()
            ->setAjaxAction($confirmedAction)
            ->setLabel($appC->translate('Delete'))
        );
        $form->addButton($W->SubmitButton()->setLabel($appC->translate('Cancel'))->addClass('widget-close-dialog'));
        $page->addItem($form);
        
        return $page;
    }
    
    public function deleteTeamMember($teamMember)
    {
        $this->requireDeleteMethod();
        $App = $this->App();
        $appC = $App->getComponentByName('Team');
        
        $recordSet = $App->TeamMemberSet();
        
        $record = $recordSet->request($teamMember);
        
        if (!$record->isDeletable()) {
            throw new \app_AccessException($appC->translate('Sorry, You are not allowed to perform this operation'));
        }
        $deletedMessage = $this->getDeletedMessage($record);
        
        if ($record->delete()) {
            $this->addMessage($deletedMessage);
        }
        
        $this->addReloadSelector('.depends-' . $record->getClassName());
        $this->addReloadSelector('.depends-teamMembers');
        $this->addReloadSelector('.depends-teamMemberRoles');
        
        return true;
    }
    
    public function memberRolesBox($teamMember, $itemId = null)
    {
        $App = $this->App();
        $W = bab_Widgets();
        
        $box = $W->FlowLayout($itemId);
        $box->addClass('depends-teamMemberRoles');
        $box->setIconFormat(16, 'left');
        $box->setReloadAction($this->proxy()->memberRolesBox($teamMember, $box->getId()));
        
        $teamMemberSet = $App->TeamMemberSet();
        $teamMemberSet->setDefaultCriteria($teamMemberSet->deleted->in(array(\app_TraceableRecord::DELETED_STATUS_DRAFT, \app_TraceableRecord::DELETED_STATUS_EXISTING)));
        
        /* @var $team Team */
        $teamMember = $teamMemberSet->get($teamMemberSet->id->is($teamMember));
        
        if(!isset($teamMember)){
            return $box;
        }
        
        $table = $App->Ui()->TeamMemberRolesTable($teamMember, $itemId);
        
        $box->addItem($table);
        return $box;
    }
    
    public function confirmDeleteTeamMemberRole($teamMemberRole)
    {
        $W = bab_Widgets();
        $App = $this->App();
        $appC = $App->getComponentByName('Team');
        $Ui = $App->Ui();
        
        $recordSet = $App->TeamMemberRoleSet();
        $record = $recordSet->get($recordSet->id->is($teamMemberRole));
        
        $page = $Ui->Page();
        
        $page->addClass('app-page-editor');
        $page->setTitle($appC->translate('Deletion'));
        
        if (!isset($record)) {
            $page->addItem(
                $W->Label($appC->translate('This element does not exists'))
            );
            $page->addClass('alert', 'alert-warning');
            return $page;
        }
        
        
        $form = new \app_Editor($App);
        
        $form->addItem($W->Hidden()->setName('id'));
        
        $recordTitle = $record->getRecordTitle();
        
        $subTitle = $appC->translate($recordSet->getDescription()) . ' "' . $recordTitle . '"';
        $form->addItem($W->Title($subTitle, 5));
        
        $form->addItem($W->Title($appC->translate('Confirm delete?'), 6));
        
        $form->addItem($W->Html(bab_toHtml($appC->translate('It will not be possible to undo this deletion'))));
        
        $confirmedAction = $this->proxy()->deleteTeamMemberRole($teamMemberRole);
        $parameters = $confirmedAction->getParameters();
        foreach ($parameters as $key => $value) {
            $form->setHiddenValue($key, $value);
        }
        $form->addButton(
            $W->SubmitButton()
            ->setAjaxAction($confirmedAction)
            ->setLabel($appC->translate('Delete'))
        );
        $form->addButton($W->SubmitButton()->setLabel($appC->translate('Cancel'))->addClass('widget-close-dialog'));
        $page->addItem($form);
        
        return $page;
    }
    
    public function deleteTeamMemberRole($teamMemberRole)
    {
        $this->requireDeleteMethod();
        $App = $this->App();
        $appC = $App->getComponentByName('Team');
        
        $recordSet = $App->TeamMemberRoleSet();
        
        $record = $recordSet->request($teamMemberRole);
        
        if (!$record->isDeletable()) {
            throw new \app_AccessException($appC->translate('Sorry, You are not allowed to perform this operation'));
        }
        $deletedMessage = $this->getDeletedMessage($record);
        
        if ($record->delete()) {
            $this->addMessage($deletedMessage);
        }
        
        $this->addReloadSelector('.depends-' . $record->getClassName());
        $this->addReloadSelector('.depends-teamMembers');
        $this->addReloadSelector('.depends-teamMemberRoles');
        
        return true;
    }
    
    
    /**
     * @param string $search
     *
     * @return void
     */
    public function suggestTeamMember($q = null)
    {
        $App = $this->App();
        $appC = $App->getComponentByName('Team');
        $contactSet = $App->ContactSet();
        $organizationSet = $App->OrganizationSet();
        
        bab_Widgets()->includePhpClass('Widget_Select2');
        $collection = new \Widget_Select2OptionsCollection();
        
        $contactCriteria = array();
        $organizationCriteria = array();
        
        if(!empty($q)){
            $contactSubCriteria = array();
            $contactSubCriteria[] = $contactSet->firstname->contains($q);
            $contactSubCriteria[] = $contactSet->lastname->contains($q);
            $contactCriteria[] = $contactSet->any($contactSubCriteria);
            
            $organizationSubCriteria = array();
            $organizationSubCriteria[] = $organizationSet->name->contains($q);
            $organizationCriteria[] = $organizationSet->any($organizationSubCriteria);
        }
        
        $contacts = $contactSet->select($contactSet->all($contactCriteria));
        foreach ($contacts as $contact){
            $orgName = null;
            $mainOrg = $contact->getMainContactOrganization();
            if($mainOrg){
                $mainOrg = $mainOrg->getMainOrganization();
                if($mainOrg){
                    $orgName = $mainOrg->getName();
                }
            }
            $collection->createOption(
                $contact->getRef(),
                sprintf($appC->translate('Contact : %s'), $contact->getFullName()),
                $orgName
            );
        }        
        
        $organizations = $organizationSet->select($organizationSet->all($organizationCriteria));
        foreach ($organizations as $organization){
            $collection->createOption(
                $organization->getRef(),
                sprintf($appC->translate('Organization : %s'), $organization->name)
            );
        }        
        
        header('Content-Type: application/json');
        die($collection->output());
    }
    
    /**
     * Return a box containing Team linked to the record given by reference. The Team records are displayed using the listItem method
     * @param string    $for    Record reference
     * @param int       $type   A TeamType id to get only the Type records with this type
     * @param int       $limit  The maximal number of Team to display
     * @param string    $itemId The
     * @return \Widget_VBoxLayout
     */
    public function listFor($for, $type = null, $limit = 5, $itemId = null)
    {
        $App = $this->App();
        $appC = $App->getComponentByName('Team');
        $W = bab_Widgets();
        
        $box = $W->VBoxLayout($itemId);
        $box->setReloadAction($this->proxy()->listFor($for, $type, $limit, $box->getId()));
        $box->addClass('depends-' . $this->getRecordClassName());
        
        $forRecord = $App->getRecordByRef($for);
        if (!isset($forRecord)) {
            return $box;
        }
        
        /* @var $teamLinkSet TeamLinkSet */
        $teamLinkSet = $App->TeamLinkSet();
        
        $conditions = array();
        $conditions[] = $teamLinkSet->objectRef->is($for);
        $conditions[] = $teamLinkSet->team()->isReadable();
        if(isset($type)){
            $conditions[] = $teamLinkSet->team()->type->is($type);
        }
        
        $teamLinks = $teamLinkSet->select(
            $teamLinkSet->all($conditions)
        );
        $teamLinks->orderDesc($teamLinkSet->start)->orderDesc($teamLinkSet->end);
        
        $nbRecords = $teamLinks->count();
        $nbItems = 0;
        foreach ($teamLinks as $record) {
            if ($nbItems >= $limit) {
                $box->addItem(
                    $W->Link(
                        sprintf($appC->translate('Show all (%d more)'), $nbRecords - $nbItems),
                        $this->proxy()->listFor($for, $type, 100)
                    )->setOpenMode(\Widget_Link::OPEN_DIALOG)
                    ->setSizePolicy('widget-align-center')
                    ->addClass('widget-actionbutton')
                );
                break;
            }
            $nbItems++;
            $listElement = $this->listItem($record);
            $box->addItem(
                $listElement->setSizePolicy('widget-list-element')
            );
        }
        return $box;
    }
    
    /**
     * Returns a \Widget_VBoxLayout containing minimal informations about the TeamLink
     * @param int $id The TeamLink id
     * @return TeamLinkItem
     */
    protected function listItem($id)
    {
        $App = $this->App();
        
        if ($id instanceof TeamLink) {
            $record = $id;
        } else {
            $record = $this->App()->TeamLinkSet()->request($id);
        }
        
        return $App->Ui()->TeamLinkItem($record);
    }
    
    
    
    /**
     * Returns a page containing an editor for the record
     * @param string $for       A \app_Record reference
     * @param int $draft        An id of a TeamLink to edit
     * @return \app_Page|\Widget_VBoxLayout
     */
    public function add($for = null, $draft = null, $reloaded = false)
    {
        $App = $this->App();
        $appC = $App->getComponentByName('Team');
        $W = bab_Widgets();
        
        /* @var $set TeamLinkSet */
        $set = $App->TeamLinkSet();
        $set->setDefaultCriteria($set->deleted->in(\app_TraceableRecord::DELETED_STATUS_EXISTING, \app_TraceableRecord::DELETED_STATUS_DRAFT));
        
        if(!$reloaded){
            $draftRecord = $this->createDraftRecord($for);
        }
        else{
            $draftRecord = $set->get($draft);
        }
        
        $linkEditor = $this->getLinkEditor($draftRecord->id);
        
        $editors = $W->VBoxItems(
            $linkEditor
        );
        
        if($reloaded){
            return $editors;
        }
        
        $page = $App->Ui()->Page();
        $page->setTitle($appC->translate('Add a new team'));
        $page->addClass('app-page-editor');
        $page->addClass('widget-no-close');
        
        $page->addItem($editors);
        return $page;
    }
    
    public function editTeamLink($teamLink)
    {
        $App = $this->App();
        $appC = $App->getComponentByName('Team');
        $W = bab_Widgets();
        
        /* @var $set TeamLinkSet */
        $set = $App->TeamLinkSet();
        $teamLink = $set->request($set->id->is($teamLink));
        
        $linkEditor = $this->getLinkEditor($teamLink->id);
        
        $editors = $W->VBoxItems(
            $linkEditor
        );
        
        $page = $App->Ui()->Page();
        $page->setTitle($appC->translate('Edit team'));
        $page->addClass('app-page-editor');
        $page->addClass('widget-no-close');
        
        $page->addItem($editors);
        return $page;
    }
    
    
    /**
     * Returns an editor for the TeamLink
     * @param int $teamLink The TeamLink record id
     * @param boolean $ajax Weither the editor will by saved in ajax or not
     * @param string $itemId The editor id
     * @return \app_Editor
     */
    public function getLinkEditor($teamLink, $ajax = true, $itemId = null)
    {
        $App = $this->App();
        $appC = $App->getComponentByName('Team');
        $W = bab_Widgets();
        
        /* @var $set TeamLinkSet */
        $set = $App->TeamLinkSet();
        $set->setDefaultCriteria($set->deleted->in(\app_TraceableRecord::DELETED_STATUS_EXISTING, \app_TraceableRecord::DELETED_STATUS_DRAFT));
        $teamLink = $set->request($set->id->is($teamLink));
        
        /* @var $teamLink TeamLink */
        
        $editor = new \app_Editor($App, $itemId);
        $editor->setIconFormat(16, 'left');
        $editor->setName('data');
        $editor->isAjax = true;
        
        $editor->addItem($W->Hidden()->setName('id'));
        
        /**
         * MAIN
         */
        $mainSection = $W->Section(
            $appC->translate('Team'),
            $mainContent = $W->VBoxItems()
        )->addClass('box');
        
        //TEAM 
        /* @var $teamSet TeamSet */
        $teamSet = $App->TeamSet();
        $genericTeams = $teamSet->select($teamSet->isGeneric->is(true))->orderAsc($teamSet->name);
            
        $teamOptions = array();
        foreach ($genericTeams as $genericTeam){
            $teamOptions[$genericTeam->id] = $genericTeam->name;
        }
        
        $editing = false;
        
        if($teamLink->deleted == \app_TraceableRecord::DELETED_STATUS_EXISTING && $team = $teamLink->team() && $teamLink->team()->deleted == \app_TraceableRecord::DELETED_STATUS_EXISTING){
            $_SESSION['team-editor-selected'] = $teamLink->team()->id;
            if($teamLink->team()->isGeneric){
                $newTeam = $teamSet->newRecord();
                $newTeam->deleted = \app_TraceableRecord::DELETED_STATUS_DRAFT;
                $newTeam->save();
                $teamOptions[$newTeam->id] = $appC->translate('Create a specific team');
                $newTeamId = $newTeam->id;
            }
            else{
                $teamOptions[$teamLink->team()->id] = $appC->translate('Create a specific team');
                $newTeamId = $teamLink->team()->id;
            }
            $editing = true;
        }
        else{
            $newTeam = $teamSet->newRecord();
            $newTeam->deleted = \app_TraceableRecord::DELETED_STATUS_DRAFT;
            $newTeam->save();
            $teamLink->team = $newTeam->id;
            $teamLink->save();
            
            $_SESSION['team-editor-selected'] = $newTeam->id;
            $teamOptions[$newTeam->id] = $appC->translate('Create a specific team');
            
            $newTeamId = $newTeam->id;
        }
        
        $mainContent->addItem(
            $W->LabelledWidget(
                $appC->translate('Team'),
                $teamSelect = $W->Select()->setOptions($teamOptions)->setAjaxAction($this->proxy()->teamEditorTeamChange(), '', 'change'),
                'team'
            )    
        );
        
        /* @var $teamTypeSet TeamTypeSet */
        $teamTypeSet = $App->TeamTypeSet();
        $teamTypes = $teamTypeSet->select()->orderAsc($teamTypeSet->name);
        
        $teamTypeOptions = array();
        foreach ($teamTypes as $teamType){
            $teamTypeOptions[$teamType->id] = $teamType->name;
        }
        $mainContent->addItem(
            $teamDetailBox = $W->VBoxItems( 
                $W->FlexItems(
                    $W->LabelledWidget(
                        $appC->translate('Team name'),
                        $W->LineEdit()->setMandatory(true, $appC->translate('The team name must be specified')),
                        array('newTeam', 'name')
                    )->addClass('team-editor-name'),
                    $W->LabelledWidget(
                        $appC->translate('Team type'),
                        $W->Select()->setOptions($teamTypeOptions),
                        array('newTeam', 'type')
                    )->addClass('team-editor-type')
                )->setGrowable()->setJustifyContent(\Widget_FlexLayout::FLEX_ALIGN_CONTENT_SPACE_BETWEEN)->setAlignItems(\Widget_FlexLayout::FLEX_ALIGN_CONTENT_FLEX_END),
                $W->LabelledWidget(
                    $appC->translate('Generic team'),
                    $W->CheckBox(),
                    array('newTeam', 'isGeneric'),
                    $appC->translate('If checked, this team will be able to be linked to other objects')
                )->addClass('team-editor-isgeneric')
            )
        );
        
        $teamSelect->setAssociatedDisplayable($teamDetailBox, array($newTeamId));
            
        $editor->addItem($mainSection);
        
        /**
         * MEMBERS
         */
        
        $memberSection = $W->Section(
            $appC->translate('Members'),
            $this->membersBoxEditor()
        )->addClass('box');
        
        $editor->addItem($memberSection);
        
        /**
         * ADDITIONAL DATA
         */
        $additionalSection = $W->Section(
            $appC->translate('Additional data'),
            $additionalContent = $W->VBoxItems()
        )->addClass('box');
        
        $additionalContent->addItems(
            $W->FlexItems(
                $W->LabelledWidget(
                    $appC->translate('Start'),
                    $W->DatePicker(),
                    'start'
                ),
                $W->LabelledWidget(
                    $appC->translate('End'),
                    $W->DatePicker(),
                    'end'
                )
            ),
            $W->LabelledWidget(
                $appC->translate('Comment'),
                $W->TextEdit(),
                'comment'
            )
        );
        
        $editor->addItem($additionalSection);
        
        
        $editor->setRecord($teamLink);
        if($editing){
            $editor->setValues(array('name' => $teamLink->team()->name, 'type', $teamLink->team()->type, 'isGeneric' => $teamLink->team()->isGeneric), array('data', 'newTeam'));
        }
        $editor->setReloadAction($this->proxy()->getLinkEditor($teamLink->id, $ajax, $editor->getId()));
        $editor->addClass('widget-no-close');
        $editor->setSaveAction($this->proxy()->saveTeamLink());
        return $editor;
    }
    
    public function saveTeamLink($data = null)
    {
        $this->requireSaveMethod();
        $App = $this->App();
        $teamSet = $App->TeamSet();
        $teamSet->setDefaultCriteria($teamSet->deleted->in(array(\app_TraceableRecord::DELETED_STATUS_DRAFT, \app_TraceableRecord::DELETED_STATUS_EXISTING)));
        
        $team = $teamSet->get($data['team']);
        if(!$team->isGeneric && isset($data['newTeam'])){
            $team->setFormInputValues($data['newTeam']);
            $team->deleted = \app_TraceableRecord::DELETED_STATUS_EXISTING;
            $team->save();
            unset($data['newTeam']);
        }
        
        $teamLinkSet = $App->TeamLinkSet();
        $teamLinkSet->setDefaultCriteria($teamLinkSet->deleted->in(array(\app_TraceableRecord::DELETED_STATUS_DRAFT, \app_TraceableRecord::DELETED_STATUS_EXISTING)));
        
        $teamLink = $teamLinkSet->get($teamLinkSet->id->is($data['id']));        
        $teamLink->setFormInputValues($data);
        $teamLink->deleted = \app_TraceableRecord::DELETED_STATUS_EXISTING;
        $teamLink->save();
        
        $this->addReloadSelector('.depends-' . $this->getRecordClassName());
        
        return true;
    }
    
    public function teamEditorTeamChange($data = null)
    {
        $_SESSION['team-editor-selected'] = $data['team'];
        $this->addReloadSelector('.depends-selectedTeam');
        return true;    
    }
    
    public function membersBoxEditor($itemId = null)
    {
        $App = $this->App();
        $W = bab_Widgets();
        $box = $W->FlowLayout($itemId);
        $box->setReloadAction($this->proxy()->membersBoxEditor($box->getId()));
        $box->addClass('depends-selectedTeam');
        $box->addClass('reload-teamMembers');
        
        if(!isset($_SESSION['team-editor-selected'])){
            return $box;
        }
        
        $selectedTeam = $_SESSION['team-editor-selected'];
        /* @var $teamSet TeamSet */
        $teamSet = $App->TeamSet();
        $teamSet->setDefaultCriteria($teamSet->deleted->in(array(\app_TraceableRecord::DELETED_STATUS_DRAFT, \app_TraceableRecord::DELETED_STATUS_EXISTING)));
        
        $selectedTeam = $teamSet->get($teamSet->id->is($selectedTeam));
        if(!isset($selectedTeam)){
            return $box;
        }
        
        $box->addItem(
            $this->membersBox($selectedTeam->id, $selectedTeam->isGeneric)->setSizePolicy('widget-100pc')
        );
        
        return $box;
    }
    
    /**
     * Creates a TeamLink record associated to the $for record with a draft status.
     * The TeamLink record is automatically saved in the database.
     * @param string $for The \app_Record reference to which the TeamLink will be linked
     * @return TeamLink
     */
    private function createDraftRecord($for)
    {
        $App = $this->App();
        
        /* @var $set TeamLinkSet */
        $set = $App->TeamLinkSet();
        
        $draftRecord = $set->newRecord();
        $draftRecord->deleted = \app_TraceableRecord::DELETED_STATUS_DRAFT;
        $draftRecord->objectRef = $for;
        $draftRecord->start = date('Y-m-d');
        
        $draftRecord->save();
        
        return $draftRecord;
    }
    
    public function confirmDeleteTeamLink($teamLink)
    {
        $W = bab_Widgets();
        $App = $this->App();
        $appC = $App->getComponentByName('Team');
        $Ui = $App->Ui();
        
        $recordSet = $App->TeamLinkSet();
        $record = $recordSet->get($recordSet->id->is($teamLink));
        
        $page = $Ui->Page();
        
        $page->addClass('app-page-editor');
        $page->setTitle($appC->translate('Deletion'));
        
        if (!isset($record)) {
            $page->addItem(
                $W->Label($appC->translate('This element does not exists'))
            );
            $page->addClass('alert', 'alert-warning');
            return $page;
        }
        
        
        $form = new \app_Editor($App);
        
        $form->addItem($W->Hidden()->setName('id'));
        
        $recordTitle = $record->getRecordTitle();
        
        $subTitle = $appC->translate($recordSet->getDescription()) . ' "' . $recordTitle . '"';
        $form->addItem($W->Title($subTitle, 5));
        
        $form->addItem($W->Title($appC->translate('Confirm delete?'), 6));
        
        $form->addItem($W->Html(bab_toHtml($appC->translate('It will not be possible to undo this deletion'))));
        
        $confirmedAction = $this->proxy()->deleteTeamLink($teamLink);
        $parameters = $confirmedAction->getParameters();
        foreach ($parameters as $key => $value) {
            $form->setHiddenValue($key, $value);
        }
        $form->addButton(
            $W->SubmitButton()
            ->setAjaxAction($confirmedAction)
            ->setLabel($appC->translate('Delete'))
            );
        $form->addButton($W->SubmitButton()->setLabel($appC->translate('Cancel'))->addClass('widget-close-dialog'));
        $page->addItem($form);
        
        return $page;
    }
    
    public function deleteTeamLink($teamLink)
    {
        $this->requireDeleteMethod();
        $App = $this->App();
        $appC = $App->getComponentByName('Team');
        
        $recordSet = $App->TeamLinkSet();
        
        $record = $recordSet->request($teamLink);
        
        if (!$record->isDeletable()) {
            throw new \app_AccessException($appC->translate('Sorry, You are not allowed to perform this operation'));
        }
        $deletedMessage = $this->getDeletedMessage($record);
        
        if ($record->delete()) {
            $this->addMessage($deletedMessage);
        }
        
        $this->addReloadSelector('.depends-' . $record->getClassName());
        $this->addReloadSelector('.depends-' . $this->getRecordClassName());
        $this->addReloadSelector('.depends-teamMembers');
        $this->addReloadSelector('.depends-teamMemberRoles');
        
        return true;
    }
}