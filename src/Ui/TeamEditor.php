<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\Team\Ui;

use Capwelton\App\Team\Set\Team;

/**
 * Team editor
 * @return TeamEditor
 */
class TeamEditor extends \app_Editor
{
    /**
     * @var Team
     */
    protected $team = null;
    
    protected $forGenericTeam = false;
    
    protected $teamComponent = null;
    
    /**
     *
     * @param \Func_App $App
     * @param Team $teamType
     * @param bool $forGenericTeam
     * @param string $id
     * @param \Widget_Layout $layout
     */
    public function __construct(\Func_App $App, Team $team = null, $forGenericTeam = false, $id = null, \Widget_Layout $layout = null)
    {
        $this->team = $team;
        $this->forGenericTeam = $forGenericTeam;
        
        $this->teamComponent = $App->getComponentByName('Team');
        
        parent::__construct($App, $id, $layout);
        $this->setName('teamType');
        
        $this->colon();
        
        $this->addFields();
        $this->addButtons();
        
        $this->setHiddenValue('tg', $App->controllerTg);
        $this->setHiddenValue('team[forGenericTeam]', $this->forGenericTeam);
        
        if (isset($team)) {
            $this->setHiddenValue('team[id]', $team->id);
            $this->setValues($team, array('team'));
        }
    }
    
    protected function addFields()
    {
        $this->addItem($this->name());
        $this->addItem($this->type());
        $this->addItem($this->members());
    }
    
    protected function addButtons()
    {
        $App = $this->App();
        $W = $this->widgets;
        
        $this->addButton(
            $submit = $W->SubmitButton()
            ->setLabel($this->teamComponent->translate('Save'))
            ->validate(true)
        );
        
        $this->addButton(
            $cancel = $W->SubmitButton()
            ->setLabel($this->teamComponent->translate('Cancel'))
        );
        
        $ctrl = $App->Controller()->Team();
        
        if(bab_isAjaxRequest()){
            $submit->setAjaxAction($ctrl->save());
            $cancel->setAjaxAction($ctrl->cancel());
        }
        else{
            $submit->setAction($ctrl->save());
            $cancel->setAction($ctrl->cancel());
        }
    }
    
    /**
     *
     * @return \Widget_Item
     */
    protected function name()
    {
        $W = $this->widgets;
        
        return $this->labelledField(
            $this->teamComponent->translate('Name'),
            $W->LineEdit()
            ->setMandatory(true, $this->teamComponent->translate('The name is mandatory'))
            ->addClass('widget-100pc'),
            'name'
        );
    }
    
    protected function type()
    {
        $App = $this->App();
        $W = $this->widgets;
        
        $teamTypeSet = $App->TeamTypeSet();
        $teamTypes = $teamTypeSet->select()->orderAsc($teamTypeSet->name);
        $options = array();
        foreach($teamTypes as $teamType){
            $options[$teamType->id] = $teamType->name;
        }
        
        return $this->labelledField(
            $this->teamComponent->translate('Type'),
            $W->Select()->setOptions($options)
            ->setMandatory(true, $this->teamComponent->translate('The type is mandatory'))
            ->addClass('widget-100pc'),
            'type'
        );
    }
    
    protected function members()
    {
        $members = $this->App()->Controller()->Team(false)->membersBox($this->team->id);
        $members->addClass('widget-100pc');
        return $members;
    }
    
    public function setValues($team, $namePathBase = array())
    {
        if ($team instanceof Team) {
            $values = $team->getFormOutputValues();
            $this->setValues(array('team' => $values));
        } else {
            parent::setValues($team, $namePathBase);
        }
    }
}