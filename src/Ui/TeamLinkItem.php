<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com})
 */

namespace Capwelton\App\Team\Ui;

use Capwelton\App\ContactOrganization\Set\Contact;
use Capwelton\App\Team\Set\TeamLink;
use Capwelton\App\Team\Set\TeamLinkSet;
use Capwelton\App\Team\Set\TeamMember;
use Capwelton\App\ContactOrganization\Set\Organization;

/**
 * @return TeamLinkItem
 * @method self addItem(Widget_Displayable_Interface $item = null)
 */
class TeamLinkItem extends \app_UiObject
{
    /**
     * @var TeamLink
     */
    protected $teamLink;
    
    /**
     * @var TeamLinkSet
     */
    protected $set;
    
    protected $ctrl;
    protected $showChip = true;
    protected $isUpdatable;
    protected $isDeletable;
    protected $Ui;
    
    protected $memberControllers = array();
    
    protected $teamComponent = null;
    
    
    public function __construct(\Func_App $App, TeamLink $teamLink, \Widget_Layout $layout = null, $itemId = null)
    {
        parent::__construct($App);
        
        $this->teamLink = $teamLink;
        $this->set = $teamLink->getParentSet();
        $this->ctrl = $App->Controller()->Team();
        $this->Ui = $App->Ui();
        
        $this->teamComponent = $App->getComponentByName('Team');
        
        $W = bab_Widgets();
        
        $this->setInheritedItem($W->Frame($itemId, $layout));
    }
    
    protected function isUpdatable()
    {
        if(!isset($this->isUpdatable)){
            $this->isUpdatable = $this->teamLink->isUpdatable();
        }
        return $this->isUpdatable;
    }
    
    protected function isDeletable()
    {
        if(!isset($this->isDeletable)){
            $this->isDeletable = $this->teamLink->isDeletable();
        }
        return $this->isDeletable;
    }
    
    protected function computeContent()
    {
        $W = bab_Widgets();
        
        $record = $this->teamLink;
        $App = $this->App();
        
        $ctrl = $this->ctrl;
        
        $team = $record->team();
        
        $chip = $team->type() ? $App->Ui()->Chip($record->team()->type()->name) : null;
        
        $title = $team->name;
        
        $listItem = $W->Section(
            $W->Label($title),
            $content = $W->VBoxLayout()
        )->setFoldable(true, false)->addClass('box red teamLinkItem');
        
        $contextMenu = $listItem->addContextMenu('inline');
        
        if ($this->isUpdatable()) {
            $contextMenu->addItem(
                $W->Link(
                    '',
                    $ctrl->editTeamLink($record->id)
                )->setOpenMode(\Widget_Link::OPEN_DIALOG)
                ->setIcon(\Func_Icons::ACTIONS_DOCUMENT_EDIT)
                ->setTitle($this->teamComponent->translate('Edit'))
                ->addClass('widget-actionbutton section-button')
                ->setDialogClass('box red')
            );
        }
        if ($this->isDeletable()) {
            $contextMenu->addItem(
                $W->Link(
                    '',
                    $ctrl->confirmDeleteTeamLink($record->id)
                )->setOpenMode(\Widget_Link::OPEN_DIALOG)
                ->setIcon(\Func_Icons::ACTIONS_EDIT_DELETE)
                ->setTitle($this->teamComponent->translate('Delete'))
                ->addClass('widget-actionbutton section-button')
                ->setDialogClass('box red')
            );
        }
        
        if($this->showChip){
            $chip = $record->team()->type() ? $App->Ui()->Chip($record->team()->type()->name) : null;
            $content->addItem(
                $chip
            );
        }
        
        $members = $team->getMembers(true);
        
        foreach ($members as $member){
            $content->addItem($this->memberItem($member));
        }
        
        $this->addItem($listItem);
    }
    
    protected function getMemberController(TeamMember $teamMember)
    {
        $member = $teamMember->member();
        if(!isset($member)){
            return null;
        }
        list($type) = explode(':', $teamMember->memberRef);
        if(!isset($this->memberControllers[$type])){
            $ctrl = $member->getController();
            $this->memberControllers[$type] = $ctrl;
        }
        
        return $this->memberControllers[$type];
    }
    
    protected function memberItem(TeamMember $teamMember)
    {
        $W = bab_Widgets();
        $App = $this->App();
        $Ui = $this->Ui;
        
        $member = $teamMember->member();
        if(!$member){
            return null;
        }
        
        $ctrl = $this->ctrl;
        
        $section = $W->Section(
            $header = $W->VBoxItems(
                $W->Label($member->getRecordTitle())->setSizePolicy('teamMemberName')
            ),
            $content = $W->VBoxLayout()
        )->setFoldable(true, true)->addClass('box magenta teamMemberItem');
        
        $allRoles = $teamMember->getRoles(true);
        
        $roleNames = array();
        foreach ($allRoles as $role){
            $teamRole = $role->teamRole();
            if(isset($teamRole)){
                $roleNames[$teamRole->id] = $teamRole->name;
            }
        }
        
        $contextMenu = $section->addContextMenu();
        if ($this->isUpdatable()) {
            $contextMenu->addItem(
                $W->Link(
                    '',
                    $ctrl->editMember($teamMember->id)
                )->setOpenMode(\Widget_Link::OPEN_DIALOG)
                ->setIcon(\Func_Icons::ACTIONS_DOCUMENT_EDIT)
                ->setTitle($this->teamComponent->translate('Edit'))
                ->addClass('widget-actionbutton section-button')
                ->setDialogClass('box magenta')
            );
        }
        if ($this->isDeletable()) {
            $contextMenu->addItem(
                $W->Link(
                    '',
                    $ctrl->confirmDeleteTeamMember($teamMember->id)
                )->setOpenMode(\Widget_Link::OPEN_DIALOG)
                ->setIcon(\Func_Icons::ACTIONS_EDIT_DELETE)
                ->setTitle($this->teamComponent->translate('Delete'))
                ->addClass('widget-actionbutton section-button')
                ->setDialogClass('box magenta')
            );
        }
        
        if($member instanceof Contact){
            $content->addItem(
                $Ui->ContactCardFrame($member)
            );
        }
        elseif($member instanceof Organization){
            $content->addItem(
                $Ui->OrganizationCardFrame($member)
            );
        }
        
        if(count($roleNames > 0)){
            $content->addItem(
                $W->Label(implode(', ', $roleNames))->setSizePolicy('teamMemberRoles')
            );
        }
        
        return $section;
    }
    
    public function showChip($showChip = true){
        $this->showChip = $showChip;
        return $this;
    }
    
    public function display(\Widget_Canvas $canvas)
    {
        $this->computeContent();
        return parent::display($canvas);
    }
}