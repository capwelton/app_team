<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\Team\Ui;

use Capwelton\App\Team\Set\Team;
use Capwelton\App\Team\Set\TeamMember;
use Capwelton\App\Team\Set\TeamLink;


class TeamUi extends \app_Ui implements \app_ComponentUi
{
    
    /**
     * Team type editor
     * @return TeamTypeEditor
     */
    public function TeamEditor(Team $record = null, $forGenericTeam = false)
    {
        return new TeamEditor($this->app, $record, $forGenericTeam);
    }
    
    /**
     * Team table view
     * @return TeamTableView
     */
    public function TeamTableView()
    {
        return new TeamTableView($this->app);
    }
    
    /**
     * Team member editor
     * @param Team $team
     * @param TeamMember $teamMember
     * @return TeamMemberEditor
     */
    public function TeamMemberEditor(TeamMember $teamMember)
    {
        return new TeamMemberEditor($this->app, $teamMember);
    }
    
    /**
     * Display the list of a team's members
     * @param Team $team
     * @param boolean $displayOnly
     * @return TeamMembersTable
     */
    public function TeamMembersTable(Team $team, $displayOnly = false)
    {
        return new TeamMembersTable($this->app, $team, $displayOnly);
    }
    
    /**
     * Display the list of a team member's roles
     * @param TeamMember $teamMember
     * @param boolean $displayOnly
     * @return TeamMemberRolesTable
     */
    public function TeamMemberRolesTable(TeamMember $teamMember, $itemId = null)
    {
        return new TeamMemberRolesTable($this->app, $teamMember, $itemId);
    }
    
    public function tableView()
    {
        return $this->TeamTableView();
    }
    
    public function editor(Team $record = null)
    {
        return $this->TeamEditor($record);
    }
    
    /**
     * @return SuggestTeamMember
     */
    public function SuggestTeamMember($id = null)
    {
        $suggest = new SuggestTeamMember($this->app, $id);
        $suggest->setDataSource($this->app->Controller()->Team()->suggestTeamMember());
        
        return $suggest;
    }
    
    /**
     * @param TeamLink $teamLink
     * @return TeamLinkItem
     */
    public function TeamLinkItem(TeamLink $teamLink)
    {
        return new TeamLinkItem($this->app, $teamLink);
    }
}
