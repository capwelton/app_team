<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\Team\Ui;

use Capwelton\App\Team\Set\Team;
use Capwelton\App\Team\Set\TeamMember;

/**
 * Team editor
 * @return TeamEditor
 */
class TeamMemberEditor extends \app_Editor
{    
    /**
     * @var TeamMember
     */
    protected $teamMember = null;
    
    protected $teamComponent = null;
    
    /**
     *
     * @param \Func_App $App
     * @param Team $teamType
     * @param bool $forGenericTeam
     * @param string $id
     * @param \Widget_Layout $layout
     */
    public function __construct(\Func_App $App, TeamMember $teamMember, $id = null, \Widget_Layout $layout = null)
    {
        $this->teamMember = $teamMember;
        $this->teamComponent = $App->getComponentByName('Team');
        
        parent::__construct($App, $id, $layout);
        $this->setName('teamMember');
        
        $this->colon();
        
        $this->addFields();
        $this->addButtons();
        
        $this->setHiddenValue('tg', $App->controllerTg);
        $this->setHiddenValue('teamMember[id]', $teamMember->id);
        $this->setValues($teamMember, array('teamMember'));
    }
    
    protected function addFields()
    {
        $this->addItem($this->suggestMember());
        $this->addItem($this->roles());
    }
    
    protected function suggestMember()
    {
        $App = $this->App();
        $box = $this->labelledField(
            $this->teamComponent->translate('Member'),
            $suggest = $App->Ui()->SuggestTeamMember()
            ->setMandatory(true, $this->teamComponent->translate('The member is mandatory'))
            ->addClass('widget-100pc'),
            'memberRef'
        );
        
        if(!empty($this->teamMember->memberRef)){
            $member = $this->teamMember->member();
            if($member){
                $suggest->addOption($member->getRef(), sprintf('%s : %s', $member->getParentSet()->getDescription(), $member->getRecordTitle()));
            }
        }
        
        return $box;
    }
    
    protected function roles()
    {
        $roles = $this->App()->Controller()->Team(false)->memberRolesBox($this->teamMember->id);
        $roles->addClass('widget-100pc');
        return $roles;
    }
    
    protected function addButtons()
    {
        $App = $this->App();
        $W = $this->widgets;
        
        $this->addButton(
            $submit = $W->SubmitButton()
            ->setLabel($this->teamComponent->translate('Save'))
            ->validate(true)
        );
        
        $this->addButton(
            $cancel = $W->SubmitButton()
            ->setLabel($this->teamComponent->translate('Cancel'))
        );
        
        $ctrl = $App->Controller()->Team();
        
        if(bab_isAjaxRequest()){
            $submit->setAjaxAction($ctrl->saveTeamMember());
            $cancel->setAjaxAction($ctrl->cancel());
        }
        else{
            $submit->setAction($ctrl->saveTeamMember());
            $cancel->setAction($ctrl->cancel());
        }
    }
    
    public function setValues($teamMember, $namePathBase = array())
    {
        if ($teamMember instanceof TeamMember) {
            $values = $teamMember->getFormOutputValues();
            $this->setValues(array('teamMember' => $values));
        } else {
            parent::setValues($teamMember, $namePathBase);
        }
    }
}