<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\Team\Ui;

use Capwelton\App\Team\Set\TeamRole;

/**
 * Team role editor
 * @return TeamRoleEditor
 */
class TeamRoleEditor extends \app_Editor
{
    protected $teamComponent = null;
    
    public function __construct(\Func_App $app, $id = null, \Widget_Layout $layout = null)
    {
        parent::__construct($app, $id, $layout);
        $this->teamComponent = $app->getComponentByName('Team');
        $this->setHiddenValue('tg', $app->controllerTg);
    }
    
    protected function prependFields()
    {
        $this->name();
    }
    
    protected function name()
    {
        $W = bab_Widgets();
        $this->addItem(
            $W->LabelledWidget(
                $this->teamComponent->translate('Name'),
                $W->LineEdit()->setSize(60),
                'name'
            )
        );
    }
}