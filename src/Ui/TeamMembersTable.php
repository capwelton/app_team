<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\Team\Ui;

use Capwelton\App\Team\Set\Team;

/**
 * Display the members of a given team
 *
 * @extends Widget_FlowLayout
 */
class TeamMembersTable extends \app_UiObject
{
    protected $team;
    protected $displayOnly = false;
    protected $ctrl;
    protected $teamComponent = null;
    
    /**
     * @param \Func_App			$app
     * @param array|\Iterator	$tags		Array or Iterator of app_Tag
     * @param string			$id
     */
    public function __construct($app, Team $team, $displayOnly = false, $id = null)
    {
        parent::__construct($app);
        $W = bab_Widgets();
        
        // We simulate inheritance from Widget_FlowLayout.
        // Now $this can be used as a FlowLayout
        $this->setInheritedItem($W->FlowLayout($id));
        
        $this->team = $team;
        $this->displayOnly = $displayOnly;
        $this->ctrl = $app->Controller()->Team();
        $this->teamComponent = $app->getComponentByName('Team');
        
        $this->computeContent();
    }
    
    protected function computeContent()
    {
        $this->addItem(
            $this->membersTable()    
        );
    }
    
    protected function membersTable()
    {
        $W = bab_Widgets();
        
        $table = $W->TableArrayView();
        $table->setPageLength(null);
        $headers = array(
            $this->teamComponent->translate('Name'),
            $this->teamComponent->translate('Type'),
            $this->teamComponent->translate('Roles')
        );
        
        if(!$this->displayOnly){
            $headers[] = $actionBox = $W->FlexLayout();
            $actionBox->addItem(
                $W->Link(
                    '',
                    $this->ctrl->addNewMember($this->team->id)
                )->setIcon(\Func_Icons::ACTIONS_LIST_ADD)->addClass('widget-actionbutton')->setOpenMode(\Widget_Link::OPEN_DIALOG)
            );
        }
        $table->setHeader($headers);
        
        $table->setSizePolicy('widget-100pc');
        
        $content = array();
        
        $teamMembers = $this->team->getMembers(true);
        
        foreach ($teamMembers as $teamMember){
            $row = array();
            
            $member = $teamMember->member();
            if(!$member){
                continue;
            }
            
            $row[] = $W->Link(
                $member->getRecordTitle(),
                $member->getController()->display($member->id)
            )->setIcon(\Func_Icons_Awesome::WEBAPP_EXTERNAL_LINK)->setOpenMode(\Widget_Link::OPEN_TAB);
            $row[] = $W->Label($member->getParentSet()->getDescription());
                
            $memberRoles = $teamMember->getRoles(true);
            $rolesBox = $W->VBoxItems();
                
            foreach ($memberRoles as $memberRole){
                $roleName = $memberRole->teamRole()->name;
                $startDate = $memberRole->start != '0000-00-00' ? bab_shortDate(bab_mktime($memberRole->start), false) : '';
                $endDate = $memberRole->end != '0000-00-00' ? bab_shortDate(bab_mktime($memberRole->end), false) : '';
                $roleStr = $roleName;
                $roleLineBox = $W->FlexItems()->setAlignItems(\Widget_FlexLayout::FLEX_ALIGN_ITEMS_BASELINE);
                if(!empty($startDate)){
                    $roleStr .= bab_nbsp().sprintf($this->teamComponent->translate('from %s'), $startDate);
                }
                if(!empty($endDate)){
                    $roleStr .= bab_nbsp().sprintf($this->teamComponent->translate('to %s'), $endDate);
                }
                $roleLineBox->addItem($W->Label($roleStr));
                if(!empty($memberRole->comment)){
                    $roleLineBox->addItem($W->Icon(bab_nbsp(), \Func_Icons::STATUS_DIALOG_QUESTION)->setTitle($memberRole->comment));
                }
                $rolesBox->addItem($roleLineBox);
            }
                
            $row[] = $rolesBox;
                
            if(!$this->displayOnly){
                $row[] = $memberActionBox = $W->HBoxItems();
                $memberActionBox->addItems(
                    $W->Link(
                        '',
                        $this->ctrl->editMember($teamMember->id)
                    )->setOpenMode(\Widget_Link::OPEN_DIALOG)->setIcon(\Func_Icons::ACTIONS_DOCUMENT_EDIT)->addClass('widget-actionbutton'),
                    $W->Link(
                        '',
                        $this->ctrl->confirmDeleteTeamMember($teamMember->id)
                    )->setOpenMode(\Widget_Link::OPEN_DIALOG)->setIcon(\Func_Icons::ACTIONS_EDIT_DELETE)->addClass('widget-actionbutton')
                    ->setTitle($this->teamComponent->translate('Delete this member'))
                )->setHorizontalSpacing(1, 'em');
            }
            $content[] = $row;
        }
        
        if(count($content) == 0){
            $content[] = array($W->Label($this->teamComponent->translate('This team does not have any member'))->setSizePolicy('alert alert-warning')->addParentAttribute('colspan', count($headers)));
        }
        
        $table->setContent($content);
        
        return $table;
    }
}