<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\Team\Ui;

use Capwelton\App\Team\Set\TeamType;

/**
 * Team type editor
 * @return TeamTypeEditor
 */
class TeamTypeEditor extends \app_Editor
{
    /**
     * @var TeamType
     */
    protected $teamType = null;
    
    protected $teamComponent = null;
    
    /**
     *
     * @param \Func_App $App
     * @param TeamType $teamType
     * @param string $id
     * @param \Widget_Layout $layout
     */
    public function __construct(\Func_App $App, TeamType $teamType = null, $id = null, \Widget_Layout $layout = null)
    {
        $this->teamType = $teamType;
        $this->teamComponent = $App->getComponentByName('Team');
        
        parent::__construct($App, $id, $layout);
        $this->setName('teamType');
        
        $this->colon();
        
        $this->addFields();
        $this->addButtons();
        
        $this->setHiddenValue('tg', $App->controllerTg);
        
        if (isset($teamType)) {
            $this->setHiddenValue('teamType[id]', $teamType->id);
            $this->setValues($teamType, array('teamType'));
        }
    }
    
    protected function addFields()
    {
        $this->addItem($this->name());
    }
    
    protected function addButtons()
    {
        $App = $this->App();
        $W = $this->widgets;
        
        $this->addButton(
            $submit = $W->SubmitButton()
            ->setLabel($this->teamComponent->translate('Save'))
            ->validate(true)
        );
        
        $this->addButton(
            $cancel = $W->SubmitButton()
            ->setLabel($this->teamComponent->translate('Cancel'))
        );
        
        $ctrl = $App->Controller()->TeamType();
        
        if(bab_isAjaxRequest()){
            $submit->setAjaxAction($ctrl->save());
            $cancel->setAjaxAction($ctrl->cancel());
        }
        else{
            $submit->setAction($ctrl->save());
            $cancel->setAction($ctrl->cancel());
        }
    }
    
    /**
     *
     * @return \Widget_Item
     */
    protected function name()
    {
        $W = $this->widgets;
        
        return $this->labelledField(
            $this->teamComponent->translate('Name'),
            $W->LineEdit()
            ->setMandatory(true, $this->teamComponent->translate('The name is mandatory'))
            ->addClass('widget-100pc'),
            'name'
        );
    }
    
    public function setValues($teamType, $namePathBase = array())
    {
        if ($teamType instanceof TeamType) {
            $values = $teamType->getFormOutputValues();
            $this->setValues(array('teamType' => $values));
        } else {
            parent::setValues($teamType, $namePathBase);
        }
    }
}