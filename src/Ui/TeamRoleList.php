<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\Team\Ui;

class TeamRoleList extends \Widget_Widget
{
    protected $App;
    
    protected $teamComponent = null;
    
    public function __construct(\Func_App $App, $id = null)
    {
        parent::__construct($id);
        $this->App = $App;
        $this->teamComponent = $App->getComponentByName('Team');
    }
    
    public function App()
    {
        return $this->App;
    }
    
    public function roleList()
    {        
        $App = $this->App();
        $W = bab_Widgets();
        
        $treeview = $W->SimpleTreeView();
        $treeview->addClass(\Func_Icons::ICON_LEFT_16);
        $treeview->setDefaultState(\Widget_SimpleTreeView::DEFAULT_STATE_EXPANDED);
        
        $set = $App->TeamRoleSet();
        $nodes = $set->select($set->parent->greaterThan('0'));
        
        $roleCtrl = $App->Controller()->TeamRole();
        
        $element = $treeview->createElement('1', '', '');
        $element->setItem($W->Label($this->teamComponent->translate('Team roles')));
        
        $treeview->appendElement($element, null);
        $element->addAction(
            'appendChild',
            $this->teamComponent->translate('Add role'),
            $GLOBALS['babSkinPath'] . 'images/Puces/edit_add.png',
            $roleCtrl->add('1')->url(),
            ''
        );
        
        foreach ($nodes as $node) {
            $element = $treeview->createElement(
                $node->id,
                '',
                $node->name,
                '',
                $roleCtrl->edit($node->id)->url()
            );
            $element->setItem(
                $W->FlowItems(
                    $W->Frame()->setCanvasOptions(
                        \Widget_Item::Options()->backgroundColor('#' . $node->color)
                    )->addClass('app-color-preview'),
                    $W->Icon($node->name, $node->icon)
                )->setVerticalAlign('middle')
                ->setHorizontalSpacing(8, 'px')
            );
            
            $element->addAction(
                'appendChild',
                $this->teamComponent->translate('Add subrole'),
                $GLOBALS['babSkinPath'] . 'images/Puces/edit_add.png',
                $roleCtrl->add($node->id)->url(),
                ''
            );
            
            $statusCanBeDeleted = true;
            if ($statusCanBeDeleted /* || bab_isUserAdministrator() */ ) {
                $element->addAction(
                    'delete',
                    $this->teamComponent->translate('Delete'),
                    $GLOBALS['babSkinPath'] . 'images/Puces/delete.png',
                    $roleCtrl->confirmDelete($node->id)->url(),
                    ''
                );
            }
            $parentId = $node->parent;
            $treeview->appendElement($element, $parentId);
        }
        $treeview->setPersistent(true);
        $treeview->setReloadPersistent(true);
        return $treeview;
    }
    
    public function display(\Widget_Canvas $canvas)
    {
        $output = parent::display($canvas);        
        $output .= $canvas->html($this->getId(), $this->getClasses(), $this->roleList());
        
        return $output;
    }
}