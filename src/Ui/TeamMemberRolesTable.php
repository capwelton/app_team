<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\Team\Ui;

use Capwelton\App\Team\Set\TeamMember;

/**
 * Display the roles of a given team member
 *
 * @extends Widget_FlowLayout
 */
class TeamMemberRolesTable extends \app_UiObject
{
    protected $teamMember;
    protected $ctrl;
    protected $id;
    protected $teamComponent;
    
    /**
     * @param \Func_App			$app
     * @param string			$id
     */
    public function __construct($app, TeamMember $teamMember, $id = null)
    {
        parent::__construct($app);
        $W = bab_Widgets();
        
        // We simulate inheritance from Widget_FlowLayout.
        // Now $this can be used as a FlowLayout
        $this->setInheritedItem($W->FlowLayout($id));
        
        $this->teamMember = $teamMember;
        $this->ctrl = $app->Controller()->Team();
        $this->id = $id;
        $this->teamComponent = $app->getComponentByName('Team');
        
        $this->computeContent();
    }
    
    protected function computeContent()
    {
        $this->addItem(
            $this->rolesTable()    
        );
    }
    
    protected function rolesTable()
    {
        $W = bab_Widgets();
        $App = $this->App();
        
        $table = $W->TableArrayView();
        $table->setPageLength(null);
        $table->setHeader(array(
            $this->teamComponent->translate('Role'),
            $this->teamComponent->translate('Start'),
            $this->teamComponent->translate('End'),
            $this->teamComponent->translate('Comment')
        ));
        
        $table->setSizePolicy('widget-100pc');
        
        $memberRoles = $this->teamMember->getRoles(true);
        
        $roleOptions = array();
        $teamRoleSet = $App->TeamRoleSet();
        $teamRoles = $teamRoleSet->select(
            $teamRoleSet->parent->isNot(0)
        )->orderAsc($teamRoleSet->name);
        foreach ($teamRoles as $teamRole){
            $roleOptions[$teamRole->id] = $teamRole->name;
        }
            
        $content = array();
        foreach ($memberRoles as $memberRole){
            $row = array();
                
            $row[] = $W->Select()->setOptions($roleOptions)->setValue($memberRole->teamRole)->setName(isset($this->id) ? array('teamMember', 'memberRoles', $memberRole->id, 'teamRole') : array('memberRoles', $memberRole->id, 'teamRole'));
            $row[] = $W->DatePicker()->setValue($memberRole->start)->setName(isset($this->id) ? array('teamMember', 'memberRoles', $memberRole->id, 'start') : array('memberRoles', $memberRole->id, 'start'));
            $row[] = $W->DatePicker()->setValue($memberRole->end)->setName(isset($this->id) ? array('teamMember', 'memberRoles', $memberRole->id, 'end') : array('memberRoles', $memberRole->id, 'end'));
            $row[] = $W->HBoxItems(
                $W->TextEdit()->setValue($memberRole->comment)->setName(isset($this->id) ? array('teamMember', 'memberRoles', $memberRole->id, 'comment') : array('memberRoles', $memberRole->id, 'comment')),
                $W->Link(
                    '',
                    $this->ctrl->confirmDeleteTeamMemberRole($memberRole->id)
                )->setOpenMode(\Widget_Link::OPEN_DIALOG)
                ->setIcon(\Func_Icons::ACTIONS_EDIT_DELETE)
                ->setTitle($this->teamComponent->translate('Delete this role'))
                ->addClass('widget-actionbutton')
            )->setHorizontalSpacing(1, 'em');
                
            $content[] = $row;
        }
            
        if(count($content) == 0){
            $content[] = array($W->Label($this->teamComponent->translate('This team member does not have any role'))->setSizePolicy('alert alert-warning')->addParentAttribute('colspan', 4));
        }
        
        $roleOptions = array(0 => '') + $roleOptions;
        $content[] = array(
            $W->LabelledWidget(
                $this->teamComponent->translate('Add a role'),
                $W->Select()->setOptions($roleOptions),
                isset($this->id) ? array('teamMember', 'memberRoles', 'new', 'teamRole') : array('memberRoles', 'new', 'teamRole')
            )->addParentAttribute('colspan', 4)
        );
        $table->setContent($content);
        
        return $table;
    }
}