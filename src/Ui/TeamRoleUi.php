<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\Team\Ui;

use Capwelton\App\Team\Set\TeamRole;


class TeamRoleUi extends \app_Ui implements \app_ComponentUi
{    
    /**
     * Team type editor
     * @return TeamRoleEditor
     */
    public function TeamRoleEditor(TeamRole $record = null)
    {
        $editor = new TeamRoleEditor($this->app, $record);
        $editor->setHiddenValue('tg', bab_rp('tg'));
        
        return $editor;
    }
    
    /**
     * Team role list
     * @return TeamRoleList
     */
    public function TeamRoleList()
    {
        return new TeamRoleList($this->app);
    }
    
    /**
     * Team role list
     * @return TeamRoleList
     */
    public function TeamRoleTableView()
    {
        return $this->TeamRoleList();
    }
    
    public function tableView()
    {
        return $this->TeamRoleTableView();
    }
    
    public function editor(TeamRole $record = null)
    {
        return $this->TeamRoleEditor($record);
    }
}
